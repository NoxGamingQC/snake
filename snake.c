/*******************************************************************************
 * $Project: $(Snake)
 * Function : Main fonction of the project
 *******************************************************************************
 * $Author: HowlNox22607 $
 * $Name: Snake $
********************************************************************************/

#include "snakeh.h"

void putapple();
void puttext(SDL_Surface **surface, char *text, int size, int r, int g, int b);
void setrect(SDL_Rect *rect, int x, int y, int w, int h);
void snakeSolo();
void menu();

SDL_Event event;
SDL_Event menuEvent;

SDL_Color textcolor;
TTF_Font *font;

SDL_Surface *tpause, *tscore, *tgameover, *tmode, *tcontinuer, *tsnake, *tchoix01, *tchoix02, *tchoix03, *tchoix04, *screen, *sprite1, *grass, *back, *apple;
SDL_Rect rtmp;
SDL_Surface *line01, *line02, *line03, *line04;
SDL_Surface *rect01, *rect02, *rect03, *rect04;
SDL_Surface *screenGameOver;
Mix_Chunk *s;
Uint32 colorkey;

int continuer = 1;
int dx = 1;
int dy = 0;
int score = 0;
int fps = 0;
int startticks, startticksf;
int gamemode = 0;
int fullscreen = 2;

char gticks = 0;
char gticksf = 0;
char cfps[100];
char cscore[100];
char m = 0;
char pause = 1;
char gameover = 0;

struct point
{
    int x;
    int y;
};
struct point snake[1000];
struct point p;
struct point papple;
int lsnake = 4;

int main(int argc, char **argv)
{
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK)!=0)
    {
        printf("Initialization value error: \"INITIALIZATION OF SDL_VIDEO_ERROR OR SDL_AUDIO_ERROR\"\n\n Reinstalling the application can fix the problem\n\n");
        return 1;
    }
    TTF_Init();
    SDL_WM_SetCaption("Snake", NULL);

    if (fullscreen == 1)
    {
        screen = SDL_SetVideoMode(WIDTH, HEIGHT, BPP, SDL_DOUBLEBUF | SDL_HWSURFACE | SDL_FULLSCREEN);
    }
    else if (fullscreen == 2)
    {
        screen = SDL_SetVideoMode(WIDTH, HEIGHT, BPP, SDL_DOUBLEBUF | SDL_HWSURFACE);
    }
    else
    {
        printf("Initialization value error: \"FULLSCREEN VALUE ERROR\"\n1) Fullscreen mode\n2) Windowed mode\n\nOther value cannot be understand by the application!\nTo fix the problem, change the wrong value in file \"settings.opt\"\n\n", stderr);
        return 1;
    }

    menu();
}


void menu()
{
    snakeSolo();
}

void snakeSolo()
{
    SDL_Joystick *joystick;

    SDL_JoystickEventState(SDL_ENABLE);
    joystick = SDL_JoystickOpen(0);

    int j,i;
    int loop = 1;
    for(j = 0 ; j < lsnake; j++)
    {
        snake[j].x = 10 + lsnake - j;
        snake[j].y = 5;
    }
    p = snake[0];

    SDL_ShowCursor(SDL_DISABLE);

    Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096);
    s = Mix_LoadWAV("Sounds/AppleSound.wav");
    if(s == NULL)
    {
        printf("Initialization value error: \"AUDIO ERROR\"\nThe applications can't read the file \"AppleSound.wav\"\n\nCheck in the application folder if there is the file that cause the error, if yes, check if the name is write correctly, else, reinstalling the application can fix the problem.\nIf the problem persist ask to the devlopper of this software\n\n");
    }

    sprite1 = SDL_LoadBMP("IMG/Corps du Snake.bmp");
    apple = SDL_LoadBMP("IMG/Pomme.bmp");
    grass = SDL_LoadBMP("IMG/Fond.bmp");
    colorkey = SDL_MapRGB(screen->format, 255, 255, 255);
    SDL_SetColorKey(sprite1, SDL_SRCCOLORKEY, colorkey);
    SDL_SetColorKey(apple, SDL_SRCCOLORKEY, colorkey);
    back = SDL_AllocSurface(SDL_HWSURFACE, WIDTH, HEIGHT, BPP, 0, 0, 0, 0);

    puttext(&tsnake, "SNAKE", 100, 72, 142, 21);
    puttext(&tpause, "Press  space  bar  to  start", 25, 255, 255, 255);
    puttext(&tscore, "Score  0", 32, 0, 0, 0);
    puttext(&tgameover, "GAME  OVER!", 80, 255, 0, 0);
    puttext(&tcontinuer, "Press  space  bar  to  continue!", 20, 255, 255, 255);
    puttext(&tmode, "Solo", 32, 0, 0, 0);

    for(i = 0; i < 40; i++)
    {
        for(j = 0; j < 30; j++)
        {
            setrect(&rtmp, i * 20, j * 20, 0, 0);
            SDL_BlitSurface(grass, NULL, back, &rtmp);
        }
    }


    setrect(&rtmp, 0, 540, 800, 600);
    SDL_FillRect(back, &rtmp, SDL_MapRGB(screen->format, 72, 142, 21));//Couleur du rectangle en bas de l'�cran de l'�cran

    //Draw Line
    SDL_Rect positionLigne01;
    positionLigne01.x = 19;
    positionLigne01.y = 9;
    line01 = SDL_CreateRGBSurface(0, WIDTH - 37, 1, 32, 0, 0, 0, 0);
    SDL_FillRect(line01, &positionLigne01, SDL_MapRGB(screen->format, 0, 0, 0));

    SDL_Rect positionLigne02;
    positionLigne02.x = 19;
    positionLigne02.y = 570;
    line02 = SDL_CreateRGBSurface(0, WIDTH - 37, 1, 32, 0, 0, 0, 0);
    SDL_FillRect(line02, &positionLigne02, SDL_MapRGB(screen->format, 0, 0, 0));

    SDL_Rect positionLigne03;
    positionLigne03.x = 19;
    positionLigne03.y = 9;
    line03 = SDL_CreateRGBSurface(0, 1, HEIGHT - 39, 32, 0, 0, 0, 0);
    SDL_FillRect(line03, &positionLigne03, SDL_MapRGB(screen->format, 0, 0, 0));

    SDL_Rect positionLigne04;
    positionLigne04.x = WIDTH - 19;
    positionLigne04.y = 9;
    line04 = SDL_CreateRGBSurface(0, 1, HEIGHT - 39, 32, 0, 0, 0, 0);
    SDL_FillRect(line04, &positionLigne04, SDL_MapRGB(screen->format, 0, 0, 0));


    if(screen==NULL) loop = 0;
    putapple();
    while(loop)
    {
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
            case SDL_QUIT:
                loop = 0;
                break;
    case SDL_JOYHATMOTION:
    if ( event.jhat.value & SDL_HAT_UP )
    {
        if(!m)
                    {
                        dx = 0;
                        dy = -1;
                    }
    }

    if ( event.jhat.value & SDL_HAT_LEFT )
    {
        if(m)
                    {
                        dx = -1;
                        dy = 0;
                    }
    }
    if ( event.jhat.value & SDL_HAT_RIGHT)
    {
        if(m)
                    {
                        dx = 1;
                        dy = 0;
                    }
    }
    if ( event.jhat.value & SDL_HAT_DOWN )
    {
        if(!m)
                    {
                        dx = 0;
                        dy = 1;
                    }
    }
    break;
            case SDL_KEYDOWN:
                if(event.key.keysym.sym == SDLK_ESCAPE || event.jbutton.button == 2) exit(EXIT_SUCCESS);
                if(event.key.keysym.sym == SDLK_DOWN || event.key.keysym.sym == SDLK_s)
                {
                    if(!m)
                    {
                        dx = 0;
                        dy = 1;
                    }
                }
                if(event.key.keysym.sym == SDLK_UP || event.key.keysym.sym == SDLK_w)
                {
                    if(!m)
                    {
                        dx = 0;
                        dy = -1;
                    }
                }
                if(event.key.keysym.sym == SDLK_RIGHT || event.key.keysym.sym == SDLK_d)
                {
                    if(m)
                    {
                        dx = 1;
                        dy = 0;
                    }
                }
                if(event.key.keysym.sym == SDLK_LEFT || event.key.keysym.sym == SDLK_a)
                {
                    if(m)
                    {
                        dx = -1;
                        dy = 0;
                    }
                }
                if(event.key.keysym.sym == SDLK_SPACE || event.jbutton.button == 1)
                {
                    pause = 1 - pause;
                    if(!pause && gameover)
                    {
                        score = 0;
                        gameover = 0;
                        sprintf(cscore, "Score  %d", score);
                        puttext(&tscore, cscore, 32, 0, 0, 0);
                        lsnake = 4;
                        for(j = 0 ; j < lsnake; j++)
                        {
                            snake[j].x = 10 + lsnake - j;
                            snake[j].y =  5;
                        }
                        p = snake[0];
                        dx = 1;
                        dy = 0;
                    }
                }
                break;

            default:
                break;
            }

        }

        if(!gticks)
        {
            startticks = SDL_GetTicks();
            gticks = 1;
        }
        if((SDL_GetTicks() - startticks) >= 50)
        {
            if(!pause && !gameover)
            {

                p.x += dx;
                p.y += dy;
                if((p.x == papple.x) && (p.y == papple.y))
                {
                    snake[lsnake] = snake[lsnake-1];
                    snake[lsnake+1] = snake[lsnake-1];
                    lsnake += 2;
                    score++;
                    sprintf(cscore, "Score  %d", score);
                    puttext(&tscore, cscore, 32, 0, 0, 0);
                    Mix_PlayChannel(-1, s, 0);
                    putapple();
                }
                m = (!dx);
                for (j = lsnake ; j > 0; j--) snake[j] = snake[j-1];
                //Limite terrain
                if(p.x < 1)  gameover = 1;
                if(p.x > 38) gameover = 1;
                if(p.y < 0)  gameover = 1;
                if(p.y > 27) gameover = 1;
                snake[0] = p;
                for(j = 1; j <= lsnake; j++)	 if((snake[0].x == snake[j].x) && (snake[0].y ==snake[j].y)) gameover = 1;
                gticks = 0;
            }
            SDL_BlitSurface(back, NULL, screen, NULL);
            //Dessiner les lignes
            SDL_BlitSurface(line01, NULL, screen, &positionLigne01);
            SDL_BlitSurface(line02, NULL, screen, &positionLigne02);
            SDL_BlitSurface(line03, NULL, screen, &positionLigne03);
            SDL_BlitSurface(line04, NULL, screen, &positionLigne04);

            //Afficher le mode de jeu
            setrect(&rtmp, WIDTH - 120, 570, 0, 0);
            SDL_BlitSurface(tmode, NULL, screen, &rtmp);

            //Position Max Hauteur pomme
            setrect(&rtmp, papple.x * 20, papple.y * 20 + 10, 0, 0);
            SDL_BlitSurface(apple, NULL, screen, &rtmp);
            for(j = 0; j < lsnake; j++)
            {
                //Position Max Hauteur snake
                setrect(&rtmp, snake[j].x * 20, snake[j].y * 20 + 10, 0, 0);
                SDL_BlitSurface(sprite1, NULL, screen, &rtmp);
            }
        }
        fps++;
        if(gticksf == 0)
        {
            startticksf = SDL_GetTicks();
            gticksf = 1;
        }
        if(SDL_GetTicks() - startticksf >= 1000)
        {
            gticksf = 0;
        }
        if(pause)
        {
            //Afficher texte pause
            SDL_Rect positionGameOver;
            positionGameOver.x = 0;
            positionGameOver.y = 0;
            screenGameOver = SDL_CreateRGBSurface(0, WIDTH, HEIGHT, 32, 0, 0, 0, 0);
            SDL_FillRect(screenGameOver, &positionGameOver, SDL_MapRGB(screen->format, 0, 0, 0));
            SDL_BlitSurface(screenGameOver, NULL, screen, &positionGameOver);

            setrect(&rtmp, 260, 180, 0, 0);
            SDL_BlitSurface(tsnake, NULL, screen, &rtmp);

            setrect(&rtmp, 250, 280, 0, 0);
            SDL_BlitSurface(tpause, NULL, screen, &rtmp);
        }
        if(gameover)
        {
            SDL_Rect positionGameOver;
            positionGameOver.x = 0;
            positionGameOver.y = 0;
            screenGameOver = SDL_CreateRGBSurface(0, WIDTH, HEIGHT, 32, 0, 0, 0, 0);
            SDL_FillRect(screenGameOver, &positionGameOver, SDL_MapRGB(screen->format, 0, 0, 0));
            SDL_BlitSurface(screenGameOver, NULL, screen, &positionGameOver);

            //Afficher texte gameover
            setrect(&rtmp, 215, 250, 0, 0);
            SDL_BlitSurface(tgameover, NULL, screen, &rtmp);
            //Afficher texte pour continuer
            setrect(&rtmp, 260, 400, 0, 0);
            SDL_BlitSurface(tcontinuer, NULL, screen, &rtmp);
            pause = 1;
        }
        //Afficher le score
        setrect(&rtmp, 30, 570, 0, 0);
        SDL_BlitSurface(tscore, NULL, screen, &rtmp);
        SDL_Flip(screen);
    }
    SDL_Quit();
    return 0;
}


void putapple()
{
    int i;
    char b = 0;
    char c = 0;
    while(!c)
    {
        b = 0;
        //Emplacement Pomme
        papple.x = (int)((rand() /(RAND_MAX / 38)) + 1);
        papple.y = (int)(rand() / (RAND_MAX / 27));

        for(i = 0; (i < lsnake) && !b; i++)
        {
            if((snake[i].x == papple.x) && (snake[i].y == papple.y))b = 1;
        }
        if(!b) c = 1;
    }
}


void puttext(SDL_Surface **surface, char *text, int size, int r, int g, int b)
{
    textcolor.r = r;
    textcolor.g = g;
    textcolor.b = b;
    font = TTF_OpenFont("Font/ArcadeClassic.ttf", size);
    if(font == NULL)
    {
        printf("Initialization value error: \"FONT ERROR\"\nThe applications can't read the file \"ArcadeClassic.ttf\"\n\nCheck in the application folder if there is the file that cause the error, if yes, check if the name is write correctly, else, reinstalling the application can fix the problem.\nIf the problem persist ask to the devlopper of this software\n\n");
    }
    SDL_FreeSurface(*surface);
    *surface = TTF_RenderText_Solid(font, text, textcolor);
    TTF_CloseFont(font);
}

void setrect(SDL_Rect *rect, int x, int y, int w, int h)
{
    rect->x = x;
    rect->y = y;
    rect->w = w;
    rect->h = h;
}
