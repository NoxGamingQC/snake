/*******************************************************************************
 * $Project: $(Snake)
 * Function : Include what the game need
 *******************************************************************************
 * $Author: HowlNox22607 $
 * $Name: Snake $
 *******************************************************************************
 *
 * Copyright 2015 by HowlNox22607
 *
********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_mixer.h>
#include <string.h>
#include <time.h>
#ifdef _WIN32
#include <windows.h>
#include <ressources.rc>
#endif

const int WIDTH = 800;
const int HEIGHT = 600;
const int BPP = 24;

